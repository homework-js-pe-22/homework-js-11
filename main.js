const buttons = document.querySelectorAll(".btn");
// console.log(buttons);
document.addEventListener("keypress", i => {
    buttons.forEach(e => {
        let currentStyle = getComputedStyle(e);
        if (currentStyle.backgroundColor === 'rgb(0, 0, 255)') {
            e.style.backgroundColor = 'rgb(0, 0, 0)';
        }
        if (i.key.toUpperCase() === e.textContent.toUpperCase()) {
            e.style.backgroundColor = 'rgb(0, 0, 255)';
        }
    });
});